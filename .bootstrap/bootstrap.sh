#! /bin/bash
## Create an ssh key and pbcopy, then log in to bitbucket and add key

##################################################
# Install command line tools 
##################################################
source ~/.bootstrap/_initialization.bootstrap

# Ask for the administrator password upfront
sudo -v
# Keep-alive: update existing `sudo` time stamp until finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
###########################################
# Bootstrap all the things!!!
###########################################
for file in ~/.bootstrap/*.bootstrap; do
  echo -e "\nRunning $file"
  source $file
done
###############################################################
# Reboot to get everything applied
###############################################################
echo "Some changes will not take effect until you reboot your machine."
read -p "Do you want to reboot your computer now? (y/N)" choice
case "$choice" in
	y | Yes | yes ) echo "Yes"; sudo reboot;;
	* ) echo  "Well, you should." ;;
esac
