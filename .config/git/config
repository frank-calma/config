    [core]
        editor = vim
        abbrev = 12
        attributesfile = ~/.config/git/attributes
        excludesfile = ~/.config/git/ignore
        autocrlf = input
        mergeoptions = --no-edit
        ignorecase = false
        pager = delta


    [user]
	name = frankcalma-forto
	email = frank.calma@forto.com

    [color]
    	branch = auto
    	diff = auto
    	status = auto
        ui = always

    [color "branch"]
        current = green bold
        local = green
        remote = yellow

    #[color "diff"]
    #    frag = magenta bold
    #    meta = 11
    #    new = green bold
    #    old = red bold
    #    commit = yellow bold
    #    whitespace = red reverse

    #[color "diff-highlight"]
    #    oldNormal = red bold
    #    oldHighlight = red bold 52
    #    newNormal = green bold
    #    newHighlight = green bold 22

    [color "status"]
        added = green reverse
        changed = yellow reverse
        untracked = red reverse

    [delta]
        plus-style = "syntax #012800"
        minus-style = "syntax #340001"
        syntax-theme = Monokai Extended
        navigate = true


    [diff "shibboleth"]
	textconv = /Users/frankcalma/Code/src/github.com/soundcloud/shibboleth-git/shibboleth-show.sh

    [ghq]
            root = ~/Code/
            root = ~/go/src

    [help]
            # Correct typos
            # autocorrect = 1

    [fetch]
	prune = true

    [difftool]
        prompt = false
    
    [mergetool]
        prompt = false
    
    [pager]
        #diff = diff-so-fancy | less --tabs=4 -RFXS --pattern '^(Date|added|deleted|modified): '
        #show = diff-so-fancy | less --tabs=4 -RFXS --pattern '^(Date|added|deleted|modified): '

    [pretty]
            custom = "%C(magenta)%h%C(red)%d %C(yellow)%ar %C(green)%s %C(yellow)(%an)"
            #                     │        │            │            │             └─ author name
            #                     │        │            │            └─ message
            #                     │        │            └─ date (relative)
            #                     │        └─ decorations (branch, heads or tags)
            #                     └─ hash (abbreviated)

    [push]
        default = simple
	autoSetupRemote = true

    [alias]
        # Repo Management
        get = -C '~/Code/' clone
        repos-backup = !git repos-list > ~//.bootstrap//repos.backup
        repos-pull-all = "!f() { ghq import < ~//.bootstrap//repos.backup;}; f"

        # Checkout
        # Checkout from all branches
        # co="!f() { git branch -a --sort=-committerdate --format='%(refname:short)' | fzf --header Checkout --preview-window right:70% --preview 'git show --color=always {}' | cut -d/ -f2- ; }; f"
        co="!f() { git branch -a --sort=-committerdate --format='%(refname:short)' | fzf --header Checkout --preview-window right:70% --preview 'git show --color=always {}' | cut -d/ -f2- | xargs git checkout; }; f"

        # Checkout local branch
        col = "!f() { branches-local --sort=-commiterdate | fzf --no-hscroll --ansi --preview-window right:70% --preview 'git show --color=always {}' | | xargs git checkout '); }; f"
        cb = checkout -b

        # Branch
        main = !git checkout main && git pull -p
        branch-name = rev-parse --abbrev-ref HEAD
        branch-diff = diff main...HEAD
        branch-files = "!git diff main...HEAD --name-status | sed '/^D/d ; s/^.\\s\\+//'"
        branches-local = for-each-ref --count=30 --sort=-committerdate refs/heads/ --format="%(refname:short)"
        branches-all = branch -a --format='%(refname)' 

        # Cleanup 
        cleanbranches = ! git branch -r --merged | grep -v main | sed 's/origin\\///' |  xargs -n 1 git branch -d; 
        branches-delete = "!f() { branches=$(git branches-local | fzf -m --preview-window right:80% --preview 'git show --color=always {}'); \
            for branch in $branches; do git branch -D $branch; done }; f"

        # Remote
        setupstreamto = ! git branch --set-upstream-to=remotes/origin/$(git rev-parse --abbrev-ref HEAD) $(git rev-parse --abbrev-ref HEAD) && git pull

        # Staging
        add-files = "! f() { files=($(git diff --name-status | grep 'M\\s' | cut -d$'\\t' -f2- | fzf -x -m -0 --preview-window right:80% --preview 'git diff --color=always $(git rev-parse --show-toplevel)/{}')); \
                    basedir=$(git rev-parse --show-toplevel); \
                    for file in "${files[@]}"; do git add \"$basedir/$file\"; done }; f"
        rm-files = "! f() { files=($(git status -s | grep ' ?? \\| D ' | fzf -x -m -0 --preview-window right:80% --preview 'git diff --color=always $(git show HEAD~1:{})' | awk '{print $2}')); \
                basedir=$(git rev-parse --show-toplevel); \
                for index in ${!files[@]}; do echo git rm "$basedir/${files[$index]}"; done }; f"

        # Reset
        # Unstage from commit list
        unstagefiles = "!f() { if [ $(git isrepo) ]; then \
            files=$(git diff --cached --name-only | fzf -x -m -0 ); if [[ -n $files ]]; then git reset HEAD -- "$files"; fi; fi; }; f"

        revertfiles = "!f() { git checkout -- $(git status -s | awk '{ print $2 }' | fzf -x -m --preview-window right:80% --preview 'git diff --color=always origin/main HEAD {}'); }; f"
        

        resetfiles = "! f() { if [ $(git isrepo) ]; then \
            files=$(git diff --name-only `git merge-base origin/main HEAD` | fzf -x -m --preview-window right:80% --preview 'git diff --color=always origin/main HEAD {}'); \
            for file in $files; do git checkout origin/main $file; done }; f"

        unstagecommitsoft = reset --soft HEAD~1                   # Undo last commit (affects HEAD only)
        unstagecommithard = reset --hard HEAD~1                  # Remove last commit (from HEAD, Index and Working Dir)

        # Commit
        c = commit
        ca = commit -a
        cm = commit -m
        amend = commit --amend
        amend-all = !git add --all && git commit --amend --reuse-message=HEAD
        # Still needs work to grab modified list
        preview-commit = !git diff --cached --name-only | fzf -x -m -0 --preview-window right:80% --preview 'git diff --color=always HEAD {}'
        un-commit = reset --soft HEAD~1

        # Merge
        m = merge

        # Pull
        up = pull -p

        # Push
        ptom = "!git push origin $(git rev-parse --abbrev-ref HEAD)"
        pom = push origin main
        create-pr = "!git ptom && git setupstreamauto && gitit"

        # Diff
        d = diff
        dc = diff --cached
        df = "!git diff-index --quiet HEAD -- || clear; git --no-pager diff --patch-with-stat"

        # Status/Logging
        isrepo = rev-parse --is-inside-work-tree
        s = status
        ss = status -sb
    	last = log -1 HEAD
        l = log --pretty=custom                      # Show custom log
        ll = log --stat --abbrev-commit
        lc = shortlog --summary --numbered           # List contributors

        # Submodules
        subpl = submodule update --init --recursive

        # Misc
        aliases = !git config --get-regexp 'alias.*' | colrm 1 6 | sed 's/[ ]/ = /'
        edit-aliases = "!f() { subl -a ~/.config/git/config; }; f"
        hub = "!f() { gitit; }; f"
[pull]
	rebase = true
[filter "lfs"]
	clean = git-lfs clean -- %f
	smudge = git-lfs smudge -- %f
	process = git-lfs filter-process
	required = true
[url "git@github.com:"]
	insteadOf = https://github.com/
    
[branch "main"]
	pushRemote = no_push
[init]
	defaultBranch = main
[credential "https://github.com"]
	helper = 
	helper = !/opt/homebrew/bin/gh auth git-credential
[credential "https://gist.github.com"]
	helper = 
	helper = !/opt/homebrew/bin/gh auth git-credential
