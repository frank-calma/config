source ~/.cli_profile

# Color ls 
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

#eval "$(rbenv init -)"

alias subash="sublime ~/.bash_profile"
alias rebash="source ~/.bash_profile"


#### Git bash completion
source /usr/local/etc/git-completion.bash
source /usr/local/etc/bash_completion

##### Brew bash completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

#GCloud bash completion
source '/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.bash.inc'
source '/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.bash.inc'

##### Shell options ######
shopt -s cdspell
shopt -s cmdhist
shopt -s lithist
shopt -s histappend

#Powerline 
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/local/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh

source .fzf.bash
#source .fzf_mv

# Trigger ~/.bashrc commands

. ~/.bashrc
export PATH="$HOME/.cargo/bin:$PATH"
